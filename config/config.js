import { resolve } from 'path'
import { bust } from '../scripts/utils'
import noop from './plugins/gulp-noop'

const env = process.env.NODE_ENV || 'development'
const WATCH = process.env.WATCH === 'true' || false
const DEV = env === 'development'
const PROD = env === 'production'

const production = cb => PROD ? cb() : noop() 
const development = cb => DEV ? cb() : noop()

const absPath = resolve(__dirname, '..', 'www')

const base = (...args) => resolve(...[absPath, ...args])

const paths = {
  base,
  mobile: {
    jsSrc: base.bind(null, 'mobile', 'js'),
    jsDist: base.bind(null, 'mobile-dist', 'development', 'js'),
    sassSrc: base.bind(null, 'mobile', 'sass'),
    sassDist: base.bind(null, 'mobile-dist', 'development', 'css'),
    iconDist: base.bind(null, 'mobile-dist', 'development', 'icon'),
    includes: base.bind(null, 'mobile', 'views', 'partials', 'assets', 'include'),
    generated: base.bind(null, 'mobile', 'views', 'partials', 'assets', 'generated')
  },
  desktop: {
    jsSrc: base.bind(null, 'includes', 'js'),
    dist: base.bind(null, 'desktop-dist', 'development'),
    jsDist: base.bind(null, 'desktop-dist', 'development', 'js'),
    sassSrc: base.bind(null, 'includes', 'sass'),
    sassDist: base.bind(null, 'desktop-dist', 'development', 'css'),
    iconDist: base.bind(null, 'desktop-dist', 'development', 'icon'),
    fractalSassSrc: base.bind(null, 'desktop', 'scss'),
    views: base.bind(null, 'se', 'templates'),
    includes: base.bind(null, 'includes')
  },
  iconSrc: base.bind(null, 'icons', 'svg'),
  email: {
    src: base.bind(null, 'email', 'src'),
    dist: base.bind(null, 'email', 'dist')
  }
}

const rev = PROD ? bust() : ''

export default {
  DEV, PROD, development, production, paths, env, WATCH,
  autoprefixer: require('./autoprefixer'),
  rev
}
