import gulp, { task, series } from 'gulp'
import del from 'del'
import sass from 'gulp-sass'
import replace from 'gulp-html-replace'
import inlineCss from 'gulp-inline-css'
import convertEncoding from 'gulp-convert-encoding'

import { log, YELLOW } from '../../scripts/logger'
import config from '../config'

const { email } = config.paths

const clean = done => {
  del(email.dist())
    .then(results => {
      if (results.length > 0 && config.DEV)
        log(`Deleted generated emails: ${results.join('\n')}`, YELLOW)
      done()
    })
}

const copy = () => {
  return gulp.src(email.src('img'))
    .pipe(gulp.dest(email.dist('img')))
}

const buildSass = () => {
  return gulp.src(email.src('scss', '**/*.scss'))
    .pipe(sass())
    .pipe(gulp.dest(email.dist('css')))
}

const buildEmail = () => {
  const cssContent = email.src('css', 'master.css')
  return gulp.src([email.src('**/*'), '!' + email.src('img/*')])
    .pipe(convertEncoding({ from: 'ISO-8859-1', to: 'UTF-8' }))
    .pipe(inlineCss({
      extraCss: cssContent,
      applyLinkTags: false,
      preserveMediaQueries: true,
      applyTableAttributes: true
    }))
    .pipe(replace('/src/', '/dist/'))
    .pipe(convertEncoding({ from: 'UTF-8', to: 'ISO-8859-1' }))
    .pipe(gulp.dest(email.dist()))
}

task('clean:email', clean)
task('copy:email-assets', copy)
task('build:email-sass', buildSass)
task('compile:email', buildEmail)
task('build:email', series('clean:email', 'build:email-sass', 'compile:email'))
