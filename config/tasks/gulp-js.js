import { task, src, dest, series, parallel } from 'gulp'
import browserify from 'browserify'
import babelify from 'babelify'
import source from 'vinyl-source-stream'
import buffer from 'vinyl-buffer'
import sourcemaps from 'gulp-sourcemaps'
import uglify from 'gulp-uglify'
import rename from 'gulp-rename'
import convertEncoding from 'gulp-convert-encoding'
import del from 'del'
import watch from '../plugins/gulp-watcher'

import { log, YELLOW, RED } from '../../scripts/logger'
import config from '../config'

const { mobile, desktop, base } = config.paths

const clean = done => {
  del([mobile.jsDist(), desktop.jsDist()])
    .then(result => {
      if (result.length > 0 && config.DEV)
        log(`Deleted javascript: ${result.join('\n')}`, YELLOW)
      done()
    })
}

const build = (filename, entry, dist, cb) => {
  return browserify({
    entries: entry,
    debug: config.DEV
  })
  .transform(babelify)
  .bundle()
  .on('error', handleError)
  .pipe(source(filename))
  .pipe(buffer())
  .pipe(config.development(sourcemaps.init))
  .pipe(config.production(uglify))
  .pipe(config.production(rename.bind(null, { suffix: config.rev })))
  .pipe(convertEncoding({ to: 'ISO-8859-1' }))
  .pipe(config.development(sourcemaps.write))
  .pipe(dest(dist))
}

function handleError (err) {
  log(`ERROR
file: ${err.filename}
line: ${err.loc.line}
column: ${err.loc.column}
message: ${err.toString()}`, RED)
  this.emit('end')
}

const buildMobile = done => build(
  'main.js',
  mobile.jsSrc('init.js'),
  mobile.jsDist()
)

const buildDesktop = done => build(
  'main.js',
  desktop.jsSrc('main.js'),
  desktop.jsDist()
) 

task('clean:js', clean)
task('build:js-mobile', buildMobile)
task('build:js-desktop', buildDesktop)

const builds = parallel('build:js-mobile', 'build:js-desktop')

task('build:js', series('clean:js', builds))

if (config.WATCH) {
  watch([mobile.jsSrc('**/*.js'), desktop.jsSrc('**/*.js')], builds) 
}

