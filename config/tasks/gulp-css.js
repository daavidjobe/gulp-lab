import { task, src, dest, series, parallel } from 'gulp'
import sass from 'gulp-sass'
import sourcemaps from 'gulp-sourcemaps'
import autoprefixer from 'gulp-autoprefixer'
import rename from 'gulp-rename'
import htmlreplace from 'gulp-html-replace'
import del from 'del'
import watch from '../plugins/gulp-watcher'

import noop from '../plugins/gulp-noop'
import { buildTemplate } from '../plugins/gulp-template'
import { log, YELLOW } from '../../scripts/logger'
import config from '../config'

const { mobile, desktop } = config.paths

const clean = done => {
  del([mobile.sassDist(), desktop.sassDist()])
    .then(result => {
      if (result.length > 0 && config.DEV)
        log(`Deleted css: ${result.join('\n')}`, YELLOW)
      done()
    })
}

const build = (source, output) => {
  return src(source)
  .pipe(config.DEV ? sourcemaps.init() : noop())
  .pipe(sass({ outputStyle: config.PROD ? 'compressed' : 'expanded' }))
  .on('error', sass.logError)
  .pipe(config.DEV ? sourcemaps.write() : noop())
  .pipe(autoprefixer(config.autoprefixer))
  .pipe(config.PROD ? rename({ suffix: config.rev }) : noop())
  .pipe(dest(output))
}


const buildMobile = () => build(
  mobile.sassSrc('**/*.scss'),
  mobile.sassDist()
)

const buildMobileTemplate = () => buildTemplate(
  mobile.includes('styles.template.html'),
  {
    'css': `/mobile-dist/${config.rev}/css/main${config.rev}.css`,
    'ie': `/mobile-dist/${config.rev}/css/ie${config.rev}.css`
  },
  mobile.generated()
)

const buildDesktop = () => build(
  desktop.sassSrc('**/*.scss'),
  desktop.sassDist()
)

const buildFractal = () => build(
  desktop.fractalSassSrc('*.scss'),
  desktop.sassDist()
)

task('clean:css', clean)
task('build:css-mobile', buildMobile)
task('build:css-mobile-template', buildMobileTemplate)
task('build:css-desktop', buildDesktop)
task('build:css-fractal', buildFractal)

task('build:css-templates', parallel('build:css-mobile-template'))

task('build:css', series(
  'clean:css', parallel(
    'build:css-mobile',
    'build:css-desktop',
    'build:css-fractal',
    'build:css-templates'
  ))
)

if (config.WATCH) {
  watch([
    desktop.sassSrc('**/*.scss'),
    mobile.sassSrc('**/*.scss'),
    desktop.fractalSassSrc('**/*.scss')
  ], parallel(
    'build:css-mobile',
    'build:css-desktop',
    'build:css-fractal'
  ))

}



