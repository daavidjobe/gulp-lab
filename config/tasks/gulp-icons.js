import gulp, { task, series, parallel } from 'gulp'
import del from 'del'
import pack from 'path'
import fs from 'fs'
import { generateSVGs as svg, generatePNGs as png } from '../plugins/gulp-svg'
import { log, YELLOW } from '../../scripts/logger'

import config from '../config'

const { mobile, desktop } = config.paths

const colors = {
  white: '#fff',
  orange: '#ff6200',
  'light-gray': '#7b7b7d',
  'dark-gray': '#333' 
}

const clean = done => {
  del([mobile.iconDist(), desktop.iconDist()])
  .then(result => {
    if (result.length > 0 && config.DEV)
     log(`Deleted icons: ${result.join('\n')}`, YELLOW)
    done()
  })
}

const generateSVGs = () => {
  return gulp.src(config.paths.iconSrc('*/.svg'))
    .pipe(svg({ colors }))
    .pipe(gulp.dest(desktop.iconDist()))
}

const generatePNGs = () => {
  const input = fs.readdirSync(config.paths.iconSrc()) 
    .filter(filename => path.extname(filename) === '.svg')
    .map(filename => desktop.iconDist(filename))
  return png({
    input, 
    output: desktop.iconDist() 
  }) 
}

const copy = () => {
 return gulp.src(desktop.iconDist('*')) 
  .pipe(gulp.dest(mobile.iconDist()))
}
task('clean:icons', clean)
task('generate:svg', generateSVGs)
task('generate:png', generatePNGs)
task('copy:icons', copy)
task('build:icons', series('clean:icons', 'generate:svg', 'generate:png', 'copy:icons'))

