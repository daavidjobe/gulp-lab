import path from 'path'
import through from 'through2'
import { convert } from 'svg-to-png'
import File from 'vinyl'
import { log, RED } from '../../scripts/logger'
const setColor = (svg, hex) => svg.replace(/<svg/g, `<svg fill="${hex}"`)
const setFilename = (filename, color) => filename.replace(/\.svg/, `--${color}.svg`)  

export const generateSVGs = options => {
  const colors = options.colors 

  return through.obj(function (file, enc, cb) {
    const data = file.contents.toString(enc)      
    const base = path.join(file.path, '..') 
    
    this.push(file)

    Object.keys(colors)
      .map( color => new File({
        base: file.base, 
        path: setFilename(file.path, color),
        contents: new Buffer(setColor(data, colors[color]))
      }))
      .forEach(created => this.push(created))

    cb()
  })
}

export const generatePNGs = options => {
  try {
    return convert(options.input, options.output, {
      defaultWidth: '48px',
      defaultHeight: '48px',
      compress: true
    })
  } catch (e) {
    return new Promise(resolve => {
      log('No PNGs generated!', RED)
      log(e, RED)
      resolve()
    })
  }
}
