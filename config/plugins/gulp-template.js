import { src, dest } from 'gulp'
import path from 'path'
import htmlreplace from 'gulp-html-replace'
import rename from 'gulp-rename'

import { log, RED } from '../../scripts/logger'

export const buildTemplate = (source, replace, dist) => {
  return src(source)
    .on('error', () => log(`${source} does not exist`, RED))
    .pipe(rename(({ basename, extname }) => {
      basename = basename.replace('.template', ''),
      extname = path.extname(source) 
    }))
    .pipe(htmlreplace(replace))
    .pipe(dest(dist))
}

