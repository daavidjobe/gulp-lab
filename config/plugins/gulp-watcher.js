import { watch } from 'gulp'
import { log, RED, YELLOW, GREEN } from '../../scripts/logger'

const ADD = 'add'
const UNLINK = 'unlink'
const CHANGE = 'change'

export default (globs, tasks) => {
  const watcher = watch(globs, tasks)
  watcher.on('change', report.bind(null, CHANGE))
  watcher.on('unlink', report.bind(null, UNLINK))
  watcher.on('add', report.bind(null, ADD))
}

const report = (eventType, path, stats) => {
  switch (eventType) {
    case ADD:
      log(`Added: ${path}`, GREEN)
    break
    case CHANGE:
      log(`Changed: ${path}`, YELLOW)
    break
    case UNLINK:
      log(`Deleted: ${path}`, RED)
    break
  }
}
