import { task, parallel, series } from 'gulp'
import requireAll from './scripts/require-all'
import { log, RED, GREEN } from './scripts/logger'
import config from './config/config'

requireAll('./config/tasks')

log(`Environment: ${config.env}`, config.PROD ? GREEN : RED)

// TODO: Add watch functionallity to sass & js
// TODO: Move this project to mobil-dev using scp

task('default', series('build:icons', parallel(
  'build:js',
  'build:css',
  'build:email'
)))
