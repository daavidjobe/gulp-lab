import { readdirSync, statSync } from 'fs'
import { resolve, basename, extname, dirname } from 'path'

const parent = module.parent
const parentDir = dirname(parent.filename)
delete require.cache[__filename]

module.exports = function requireAll (directory, options) {
  const dir = directory || '.'
  const opts = options || {}

  const absDir = resolve(parentDir, dir)

  let files = readdirSync(absDir)
    .map(file => {
      const ext = extname(file)
      return {
        name: basename(file, ext),
        path: resolve(absDir, file),
        ext
      } 
    })
    .filter(file => file.ext === '.js')
    
    files.forEach(file => {
      if (statSync(file.path).isDirectory()) {
        files = [
          ...files,
          requireAll(file.path, opts)
        ]     
      }
    })
  return files.map(file => require(file.path))
}
