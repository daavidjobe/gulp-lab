export const RED = '\x1b[31m'
export const GREEN = '\x1b[32m'
export const YELLOW = '\x1b[33m'
export const BLUE = '\x1b[34m'

const CLEAR = '\x1b[0m'

export const log = (message, color) => {
  console.log(`${color}${message}${CLEAR}`)
}
